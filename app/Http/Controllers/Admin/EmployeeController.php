<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view("admin.employee.list");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['accion'] = "create";
        $data['title']  = "Crear Empleado";
        $data['btn_action'] = "Guardar";

        $companies = Company::all();


        return view("admin.employee.form",compact('data','companies'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployeeRequest $request)
    {
        //

        $form = $request->validated();

        $employee = Employee::create($form);

        return redirect("admin/companies/$employee->company_id/edit");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entry = Employee::findOrFail($id);

        $companies = Company::all();

        $data['accion'] = "edit";
        $data['title']  = "Editar Empleado";
        $data['btn_action'] = "Actualizar";


        return view("admin.employee.form", compact('entry','data','companies'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployeeRequest $request, $id)
    {

        $employee = Employee::findOrFail($id);

        $form = $request->validated();

        $employee->update($form);

        //dd($employee);

        return redirect("admin/companies/$employee->company_id/edit");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $employee = Employee::findOrFail($id);
        $employee->delete();

        return redirect("admin/companies/$employee->company_id/edit");
    }
}
