<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;
use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view("admin.company.list");

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['accion'] = "create";
        $data['title']  = "Crear Compañia";
        $data['btn_action'] = "Guardar";

        return view("admin.company.form",compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCompanyRequest $request)
    {
        //

        $form = $request->validated();

        $company = Company::create($form);

        if($request->file('logo')){

            $image_name = "company_$company->id.".$request->logo->extension();
            $request->logo->move(storage_path('app/public/logos'), $image_name);
            $company->update(['logo'=>"logos/$image_name"]);

        }

        return redirect('admin/companies');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entry = Company::findOrFail($id);

        $data['accion'] = "show";
        $data['title']  = "Ver Compañia";



        return view("admin.company.show", compact('entry','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $entry = Company::findOrFail($id);

        $data['accion'] = "edit";
        $data['title']  = "Editar Compañia";
        $data['btn_action'] = "Actualizar";


        return view("admin.company.form", compact('entry','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompanyRequest $request, $id)
    {
        //

        //dd($request->all());

        $company = Company::findOrFail($id);

        $form = $request->validated();

        if($request->file('logo')){

            $image_name = "company_$id.".$request->logo->extension();
            $request->logo->move(storage_path('app/public/logos'), $image_name);
            $form['logo'] = "logos/$image_name";

        }

        $company->update($form);

        //dd($company);

        return redirect('admin/companies');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
