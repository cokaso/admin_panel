<?php

namespace App\Http\Livewire\Employee;

use App\Models\Employee;
use Livewire\Component;

class BtnDelete extends Component
{
    public $employee_id;
    public $employee;

    protected $listeners =[
        'deleteEmploye'
    ];

    public function mount($employee){
        $this->employee = $employee;
        $this->employee_id = $employee->id;
    }

    public function deleteConfirm($employee_id){

        $full_name =$this->employee->first_name." ".$this->employee->last_name;

        $this->dispatchBrowserEvent('SwalConfirm',[
            'title' => 'Eliminar Empleado',
            'html'  => "¿Desea eliminar al empleado <strong>$full_name ?</strong>",
            'id'    => $employee_id
        ]);


    }

    public function deleteEmploye($id){

        $delete = Employee::find($id)->delete();
        if($delete){
            $this->dispatchBrowserEvent('deleted');
        }

    }

    public function render()
    {
        return view('livewire.employee.btn-delete');
    }

}
