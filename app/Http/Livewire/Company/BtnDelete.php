<?php

namespace App\Http\Livewire\Company;


use App\Models\Company;
use App\Models\Employee;
use Livewire\Component;

class BtnDelete extends Component
{
    public $company_id;
    public $company;

    protected $listeners =[
        'deleteCompany'
    ];

    public function mount($company){
        $this->company = $company;
        $this->company_id = $company->id;
    }

    public function deleteConfirm($company_id){

        $full_name =$this->company->name;

        $this->dispatchBrowserEvent('SwalConfirm',[
            'title' => 'Eliminar Compañia',
            'html'  => "¿Desea eliminar la Compañia <strong>$full_name ?</strong>",
            'id'    => $company_id
        ]);


    }

    public function deleteCompany($id){

        $empleados = Employee::where("company_id",$id)->get();
        if(isset($empleados)){
            foreach ($empleados as $e){
                $e->delete();
            }
        }

        $company    = Company::find($id);
        if($company){
            $company->delete();
            $this->dispatchBrowserEvent('deletedCompany');
        }

    }


    public function render()
    {
        return view('livewire.company.btn-delete');
    }
}
