<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\Company;
use Rappasoft\LaravelLivewireTables\Views\Columns\ImageColumn;


class CompanyTable extends DataTableComponent
{
    protected $model = Company::class;

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    protected $listeners =[
        'refreshList'
    ];

    public function refreshList(){
        //$this->render();
    }

    public function columns(): array
    {
        return [
            Column::make("Id", "id")
                ->sortable(),

            Column::make(__("Nombre"), "name")
                ->sortable()
                ->searchable(),

            Column::make(__("Email"), "email")
                ->sortable()
                ->searchable(),

            Column::make(__("Website"), "website")
                ->sortable()
                ->searchable()
                ->format(
                    fn($value, $row, Column $column) => Str::limit($row->website,30)
                ),
            Column::make(__("Fecha creacion"), "created_at")
                ->sortable()
                ->format(
                    fn($value, $row, Column $column) => Carbon::create($row->created_at)->format("d/m/Y H:i")
                ),
            Column::make(__("Fecha actualización"), "updated_at")
                ->sortable()
                ->format(
                    fn($value, $row, Column $column) => Carbon::create($row->updated_at)->format("d/m/Y H:i")
                ),

            Column::make('Actions','id')
                ->view('admin.company.actions')
                ->html()->excludeFromColumnSelect(),

        ];
    }
}
