<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\Employee;

class EmployeeTable extends DataTableComponent
{
    protected $model = Employee::class;

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function columns(): array
    {
        return [
            Column::make("Id", "id")
                ->sortable(),
            Column::make(__("Compañia"), "company.name")
                ->sortable(),
            Column::make(__("Nombre"), "first_name")
                ->sortable()
                ->searchable(),
            Column::make(__("Apellido"), "last_name")
                ->sortable()
                ->searchable(),
            Column::make(__("Email"), "email")
                ->sortable()
                ->searchable(),
            Column::make(__("Telefono"), "phone")
                ->sortable()
                ->searchable(),
            Column::make(__("Fecha creacion"), "created_at")
                ->sortable()
                ->format(
                    fn($value, $row, Column $column) => Carbon::create($row->created_at)->format("d/m/Y H:i")
                ),
            Column::make(__("Fecha actualización"), "updated_at")
                ->sortable()
                ->format(
                    fn($value, $row, Column $column) => Carbon::create($row->updated_at)->format("d/m/Y H:i")
                ),
            Column::make('Actions','id')
                ->view('admin.employee.actions')
                ->html()->excludeFromColumnSelect(),
        ];
    }
}
