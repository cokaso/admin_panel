<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => "required|unique:companies,name,".$this->id."|max:255",
            'email'   => "required|unique:companies,email,".$this->id,
            'logo'    => "nullable|image|mimes:jpg,jpeg,png,gif|max:2048",
            'website' => "nullable"
        ];
    }
}
