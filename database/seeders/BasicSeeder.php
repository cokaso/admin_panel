<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class BasicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create(['name'=>'Admin','email'=>'admin@admin.com','password'=>bcrypt('password')]);

        Storage::deleteDirectory('public/logos');
        Storage::makeDirectory('public/logos');

        $companies = Company::factory(10)->create();

        foreach ($companies as $company){

            $employess = Employee::factory(rand(1,5))->create([
                'company_id' => $company->id
            ]);

        }


    }
}
