<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $logo_url = "logos/".$this->faker->image('public/storage/logos',100,100, null, false);

        return [
            'name'  => $this->faker->company,
            'email' => $this->faker->unique()->email,
            'logo'  => $logo_url,
            'website' => $this->faker->url
        ];
    }
}
