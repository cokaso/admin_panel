@extends('adminlte::page')

@section('title', 'Compañías')

@section('content_header')
    <h1>Compañías</h1>
@stop

@section('content')

    <p>Lista de Compañias</p>

    <div class="row mb-3">
        <div class="col-md-4">
            <a href="{{ url("admin/companies/create")}}" class="btn btn-primary form-inline">
                <i class="fa fa-plus mr-1"></i> Adicionar Compañia
            </a>
        </div>
    </div>

    <livewire:company-table/>

@stop

@section('css')
    <link  href="{{ asset("vendor/sweetalert2/sweetalert2.min.css") }}" rel="stylesheet" />
@stop

@section('js')
    <script src="//unpkg.com/alpinejs" defer></script>
    <script src="{{ asset("vendor/sweetalert2/sweetalert2.min.js") }}"></script>
    <script>

        window.addEventListener('SwalConfirm', function(event){
            swal.fire({
                title:event.detail.title,
                imageWidth:48,
                imageHeight:48,
                html:event.detail.html,
                showCloseButton:true,
                showCancelButton:true,
                cancelButtonText:'Cancelar',
                confirmButtonText:'Sí, Eliminar',
                cancelButtonColor:'#d33',
                confirmButtonColor:'#3085d6',
                width:450,
                allowOutsideClick:false
            }).then(function(result){
                if(result.value){
                    window.livewire.emit('deleteCompany',event.detail.id);
                }
            })
        })

        window.addEventListener('deletedCompany', function(event){
            //window.livewire.emit('refreshList',event.detail.id);
            location.reload();

        });

    </script>
@stop
