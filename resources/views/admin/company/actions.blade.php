<div class="btn-group" role="group" aria-label="Acciones">

    <a href="{{ route("admin.companies.show",$row) }}" class="btn btn-sm btn-default">
        <i class="fas fa-fw fa-eye"></i>
    </a>

    <a href="{{ route("admin.companies.edit",$row) }}" class="btn btn-sm btn-primary">
        <i class="fas fa-fw fa-edit"></i>
    </a>

    @livewire("company.btn-delete",['company' => $row])

</div>
