@extends('adminlte::page')

@section('title',$data['title'])

@section('content_header')
    <h1>{{$data['title']}}</h1>
@stop

@section('content')

    @if(isset($entry))
        <form method="POST"  action="{{ url("admin/companies/".$entry->id) }}" enctype="multipart/form-data" >
        @method('PUT')
    @else
        <form method="POST" action="{{ url("admin/companies") }}" enctype="multipart/form-data" >
    @endif

        @csrf

        <input type="hidden" id="id" name="id" value="{{ isset($entry)?$entry->id:0 }}" />


        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">Detalle</h3>
            </div>
            <div class="card-body">

                <div class="form-group">
                    <label for="name">Nombre</label>
                    <x-adminlte-input name="name" type="name" value="{{ isset($entry)?$entry->name:'' }}" placeholder="Ej.: ABC Company"  required/>
                </div>

                <div class="form-group">
                    <label for="email">Correo</label>
                    <x-adminlte-input name="email" type="email" value="{{ isset($entry)?$entry->email:'' }}" placeholder="Ej.: jsmith@company.com" required/>
                </div>

                <x-adminlte-input-file name="logo" label="Logo (100 x 100)" placeholder="Choose a file..."/>

                <div class="form-group">
                    <label for="email">Pagina Web</label>
                    <x-adminlte-input name="website" type="text" value="{{ isset($entry)?$entry->website:'' }}" placeholder="Ej.: https://www.company.com" />
                </div>
            </div>

        </div>

        <button type="submit" class="btn btn-success"> <i class="fa fa-save mr-1"></i> {{$data['btn_action']}}</button>

    </form>

    @if(isset($entry))

    <hr>

    <h3>Lista de Empleados</h3>

    <div class="row mb-3">
        <div class="col-md-4">
            <a href="{{ url("admin/employees/create")}}" class="btn btn-primary form-inline">
                <i class="fa fa-plus mr-1"></i> Adicionar Empleado
            </a>
        </div>
    </div>

    <livewire:company-employees-table :company_id="$entry->id"/>

    @endif


@stop

@section('css')
    <link  href="{{ asset("vendor/sweetalert2/sweetalert2.min.css") }}" rel="stylesheet" />
@stop

@section('js')

    <script src="//unpkg.com/alpinejs" defer></script>
    <script src="{{ asset("vendor/sweetalert2/sweetalert2.min.js") }}"></script>

<script>

    window.addEventListener('SwalConfirm', function(event){
        swal.fire({
            title:event.detail.title,
            imageWidth:48,
            imageHeight:48,
            html:event.detail.html,
            showCloseButton:true,
            showCancelButton:true,
            cancelButtonText:'Cancelar',
            confirmButtonText:'Sí, Eliminar',
            cancelButtonColor:'#d33',
            confirmButtonColor:'#3085d6',
            width:450,
            allowOutsideClick:false
        }).then(function(result){
            if(result.value){
                window.livewire.emit('deleteEmploye',event.detail.id);
            }
        })
    })

    window.addEventListener('deleted', function(event){
        //window.livewire.emit('refreshList',event.detail.id);
        location.reload();
    });

</script>


@stop
