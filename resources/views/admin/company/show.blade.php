@extends('adminlte::page')

@section('title', 'Compañías')

@section('content_header')
    <h1>Companía <span class="text-blue">'{{$entry->name}}'</span></h1>
@stop

@section('content')

    <div class="row mb-3">
        <div class="col-md-4">
            <a href="{{ url("admin/companies/$entry->id/edit")}}" class="btn btn-primary form-inline">
                <i class="fas fa-fw fa-edit mr-1"></i> Editar Compañia
            </a>
        </div>
    </div>


    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Detalle</h3>
        </div>
        <form>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <img id="logo" src="{{ Storage::url($entry->logo) }}"
                                 class="product-image w-25 mb-2 img-thumbnail"
                                 alt="Logo {{$entry->name}}">
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nombre</label>
                            <span class="form-control" >{{$entry->name}}</span>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <span class="form-control" >{{$entry->email}}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Pagina web</label>
                            <span class="form-control" >{{$entry->website}}</span>
                        </div>
                    </div>
                </div>


            </div>
            <div class="card-footer">

            </div>
        </form>
    </div>

    <hr>

    <h3>Lista de Empleados</h3>

    <livewire:company-employees-table :company_id="$entry->id"/>

@stop

@section('css')

@stop

@section('js')
    <script src="//unpkg.com/alpinejs" defer></script>
@stop
