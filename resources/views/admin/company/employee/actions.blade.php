<div class="btn-group" role="group" aria-label="Acciones">

    <a href="{{ route("admin.employees.edit",$row) }}" class="btn btn-sm btn-primary">
        <i class="fas fa-fw fa-edit"></i>
    </a>

    @livewire("employee.btn-delete",['employee' => $row])

</div>
