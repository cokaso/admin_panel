@extends('adminlte::page')

@section('title',$data['title'])

@section('content_header')
    <h1>{{$data['title']}}</h1>
@stop

@section('content')

@if(isset($entry))
    <form method="POST"  action="{{ url("admin/employees/".$entry->id) }}" enctype="multipart/form-data" >
    @method('PUT')
@else
    <form method="POST" action="{{ url("admin/employees") }}" enctype="multipart/form-data" >
        @endif

        @csrf

        <input type="hidden" id="id" name="id" value="{{ isset($entry)?$entry->id:0 }}" />

        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">Detalle</h3>
            </div>
            <div class="card-body">

                <div class="form-group">
                    <label for="name">Compañia</label>
                    <select class="form-control" name="company_id" id="company_id">
                        @foreach($companies as $line)
                            <option value="{{$line->id}}" @if(isset($entry) and $entry->company_id == $line->id) selected="selected" @endif >{{$line->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <x-adminlte-input label="Nombre" name="first_name" type="text" value="{{ isset($entry)?$entry->first_name:'' }}" placeholder="Ej.: John"  required/>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <x-adminlte-input label="Apellido" name="last_name" type="text" value="{{ isset($entry)?$entry->last_name:'' }}" placeholder="Ej.: Smith" required/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <x-adminlte-input label="Correo" name="email" type="email" value="{{ isset($entry)?$entry->email:'' }}" placeholder="Ej.: jsmith@company.com"  required/>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <x-adminlte-input label="Telefono" name="phone" type="text" value="{{ isset($entry)?$entry->phone:'' }}" placeholder="" />
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <button type="submit" class="btn btn-success"> <i class="fa fa-save mr-1"></i> {{$data['btn_action']}}</button>

    </form>



@stop

@section('css')

@stop

@section('js')

    <script src="//unpkg.com/alpinejs" defer></script>
    <script type="text/javascript">

    </script>

@stop
