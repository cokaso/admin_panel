@extends('adminlte::page')

@section('title', 'Empleados')

@section('content_header')
    <h1>Empleados</h1>
@stop

@section('content')

    <p>Lista de Empleados</p>

    <livewire:employee-table/>

@stop

@section('css')

@stop

@section('js')
    <script src="//unpkg.com/alpinejs" defer></script>
    <script> console.log('Hi!'); </script>
@stop
