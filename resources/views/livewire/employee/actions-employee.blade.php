<div class="btn-group" role="group" aria-label="Acciones">

    <button data-toggle="modal" data-target="#exampleModal" wire:click="editModal({{ $employee_id }})" class="btn btn-sm btn-primary">
        <i class="fas fa-fw fa-edit"></i>
    </button>

    <a href="{{ route("admin.employees.destroy",$employee_id) }}"class="btn btn-sm btn-danger">
        <i class="fas fa-fw fa-trash"></i>
    </a>
</div>
