<button class="btn btn-sm btn-danger" wire:click="deleteConfirm({{$this->employee_id}})">
    <i class="fas fa-fw fa-trash"></i>
</button>
