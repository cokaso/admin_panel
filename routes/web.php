<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes(['register' => false]);

Route::middleware("auth")
    ->prefix("admin")
    ->as("admin.")
    ->group( function () {

        Route::get("/",  [App\Http\Controllers\Admin\IndexController::class,'index'])->name("index");

        Route::resource('companies', \App\Http\Controllers\Admin\CompanyController::class);
        Route::resource('employees', \App\Http\Controllers\Admin\EmployeeController::class);

    });

